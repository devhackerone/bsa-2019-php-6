<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetMostPopularProductAction
{
    private $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function execute(): GetMostPopularProductResponse
    {
        $product = $this->repository->findAll();

        $rating = 0;
        $mostPopular = 0;
        foreach ($product as $key => $id) {
            if ($id->getRating() > $rating) {
                $rating = $id->getRating();
                $mostPopular = $key;
            }
        }

        return new GetMostPopularProductResponse($product[$mostPopular]);
    }
}