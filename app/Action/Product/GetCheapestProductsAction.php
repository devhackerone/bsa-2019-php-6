<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;
use App\Entity\Product;

class GetCheapestProductsAction
{
    const CHEAPEST_PRODUCTS = 3;
    private $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }
    
    public function execute(int $count = self::CHEAPEST_PRODUCTS): GetCheapestProductsResponse
    {
        $products = $this->repository->findAll();

        uasort($products, function (Product $a, Product $b) {
            return $a->getPrice() > $b->getPrice();
        });

        $products = array_slice($products, 0, $count);

        return new GetCheapestProductsResponse($products);
    }
}