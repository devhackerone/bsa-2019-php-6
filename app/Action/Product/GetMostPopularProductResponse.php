<?php

declare(strict_types=1);

use App\Entity\Product;

namespace App\Action\Product;

use App\Entity\Product;

class GetMostPopularProductResponse
{
    private $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }
}