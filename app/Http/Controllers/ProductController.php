<?php

namespace App\Http\Controllers;

use App\Repository\ProductRepositoryInterface;
use App\Http\Presenter\ProductArrayPresenter;
use App\Action\Product\GetMostPopularProductAction;
use App\Action\Product\GetCheapestProductsAction;

class ProductController extends Controller
{
    private $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function getAllProducts()
    {
        $products = $this->repository->findAll();
        $presenterProducts = [];
        foreach ($products as $product) {
            $presenterProducts[] = ProductArrayPresenter::present($product);
        }

        return response()->json($presenterProducts);
    }

    public function popular()
    {
        $repository = $this->repository;
        $container = new GetMostPopularProductAction($repository);
        $product = $container->execute()->getProduct();

        return response()->json(ProductArrayPresenter::present($product));
    }

    public function cheap()
    {
        $repository = $this->repository;
        $container = new GetCheapestProductsAction($repository);
        $products = $container->execute()->getProducts();

        $presenterProducts = [];
        foreach ($products as $product) {
            $presenterProducts[] = ProductArrayPresenter::present($product);
        }

        return view('cheap_products', ['products' => $presenterProducts]);
    }
}